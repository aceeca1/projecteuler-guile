(define (prime-table n)
    (let loop ((a (make-vector (+ n 1) 0)) (u 0) (i 2))
        (if (< n i) (begin (vector-set! a u (+ n 1)) a)
            (let ((v (vector-ref a i)))
                (if (zero? v) (begin
                    (vector-set! a u i)
                    (set! u i)
                    (set! v i)))
                (let loop ((w 2))
                    (if (<= (* i w) n) (begin
                        (vector-set! a (* i w) w)
                        (if (< w v) (loop (vector-ref a w))))))
                (loop a u (+ i 1))))))

(display (let* (
    (n 10000)
    (m (inexact->exact (ceiling (* n (log (* 2.0 n (log n)))))))
    (a (prime-table m)))
    (let loop ((n n) (i 2))
        (if (zero? n) i (loop (- n 1) (vector-ref a i))))))
(newline)
