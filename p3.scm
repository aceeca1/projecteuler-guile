(display (let loop ((n 600851475143) (i 2) (answer 0)) (cond
    ((> (* i i) n) (max answer n))
    ((zero? (modulo n i)) (loop (quotient n i) i i))
    (else (loop n (+ i 1) answer)))))
(newline)
