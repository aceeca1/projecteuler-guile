(define (next n) (if (even? n) (quotient n 2) (+ n n n 1)))
(define n 1000000)
(define a (make-vector (+ n 1) 0))
(vector-set! a 1 1)

(define (collatz-len v)
    (define (answer) (+ (collatz-len (next v)) 1))
    (define (av) (vector-ref a v))
    (cond ((< n v) (answer)) ((not (zero? (av))) (av)) (else
        (let ((answer (answer))) (vector-set! a v answer) answer))))

(display (let loop ((i 1) (answer 0))
    (if (< n i) answer (loop (+ i 1)
        (if (< (vector-ref a answer) (collatz-len i)) i answer)))))
(newline)
