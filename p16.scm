(display (let* (
    (m (let loop ((n 1000) (answer 1))
        (if (zero? n) answer (loop (- n 1) (+ answer answer)))))
    (m (string->list (number->string m)))
    (m (map (lambda (c) (- (char->integer c) (char->integer #\0))) m)))
    (apply + m)))
(newline)
