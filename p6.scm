(display (let loop ((i 1) (answer1 0) (answer2 0))
    (if (< 100 i) (- (* answer2 answer2) answer1)
        (loop (+ i 1) (+ answer1 (* i i)) (+ answer2 i)))))
(newline)
