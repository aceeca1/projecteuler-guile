(display (let* (
    (m (let loop ((i 2) (answer 1))
        (if (< 100 i) answer (loop (+ i 1) (* answer i)))))
    (m (string->list (number->string m)))
    (m (map (lambda (c) (- (char->integer c) (char->integer #\0))) m)))
    (apply + m)))
(newline)

