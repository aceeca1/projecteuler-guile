(define (choose n k)
    (let loop ((i 1) (answer 1))
        (if (< k i) answer
            (loop (+ i 1) (quotient (* answer (- n i -1)) i)))))

(display (choose 40 20))
(newline)
