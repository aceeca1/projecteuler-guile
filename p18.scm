(use-modules (rnrs io ports))

(define (string-split s)
    (define (find-first-space k) (cond
        ((<= (string-length s) k) k)
        ((char=? (string-ref s k) #\space) k)
        (else (find-first-space (+ k 1)))))
    (define answer (list 'head))
    (let loop ((i 0) (tail answer))
        (if (<= (string-length s) i) (cdr answer)
            (let ((i2 (find-first-space i)))
                (set-cdr! tail (list (substring s i i2)))
                (loop (+ i2 1) (cdr tail))))))

(define (string->ints s)
    (list->vector (map string->number (string-split s))))

(define (reduce f a)
    (let loop ((ar (car a)) (dr (cdr a)))
        (if (not (pair? dr)) ar (loop (f ar (car dr)) (cdr dr)))))

(define (main port)
    (define n 15)
    (define a (map
        (lambda (i) (string->ints (get-line port)))
        (vector->list (make-vector n))))
    (define (f ar adr)
        (define m (vector-length ar))
        (define ar- (make-vector (+ m 1)))
        (let loop ((i 0)) (if (< m i) ar- (begin
            (vector-set! ar- i (+ (vector-ref adr i) (cond
                ((= i 0) (vector-ref ar i))
                ((= i m) (vector-ref ar (- i 1)))
                (else (max (vector-ref ar i) (vector-ref ar (- i 1)))))))
            (loop (+ i 1))))))
    (define answer (reduce f a))
    (display (apply max (vector->list answer)))
    (newline))

(call-with-input-file "data/input18.txt" main)
