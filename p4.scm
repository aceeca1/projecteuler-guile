(define (palindrome? s)
    (let ((a (string->list s))) (equal? a (reverse a))))

(display (let loop ((i1 999) (answer 0))
    (if (< i1 100) answer
        (loop (- i1 1)
            (let loop ((i2 999) (answer answer))
                (if (< i2 100) answer
                    (let* ((product (* i1 i2)) (s (number->string product)))
                        (if (palindrome? s)
                            (max answer product)
                            (loop (- i2 1) answer)))))))))
(newline)
