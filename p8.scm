(use-modules (rnrs io ports))

(define (make-productz)
    (define product 1)
    (define zero 0)
    (define (operate op1 op2 c)
        (if (char=? #\0 c)
            (set! zero (op1 zero 1))
            (set! product (op2 product
                (- (char->integer c) (char->integer #\0))))))
    (lambda (op . opts) (case op
        ('add (operate + * (car opts)))
        ('del (operate - quotient (car opts)))
        ('product (if (zero? zero) product 0)))))

(define (main port)
    (define answer 0)
    (define pz (make-productz))
    (define s (apply string-append (map
        (lambda (i) (get-line port))
        (vector->list (make-vector 20)))))
    (let loop ((i 0))
        (if (< i 13) (begin
            (pz 'add (string-ref s i))
            (loop (+ i 1)))))
    (let loop ((i 13))
        (set! answer (max answer (pz 'product)))
        (if (< i (string-length s)) (begin
            (pz 'add (string-ref s i))
            (pz 'del (string-ref s (- i 13)))
            (loop (+ i 1)))))
    (display answer)
    (newline))

(call-with-input-file "data/input8.txt" main)
