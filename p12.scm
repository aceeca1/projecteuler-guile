(define (divisor-num n)
    (define answer 0)
    (let loop ((i 1)) (cond
        ((< (* i i) n) (begin
            (if (zero? (modulo n i))
                (set! answer (+ answer 2)))
            (loop (+ i 1))))
        ((= (* i i) n) (+ answer 1))
        (else answer))))

(display (let loop ((n 1) (m 2))
    (if (< 500 (divisor-num n)) n (loop (+ n m) (+ m 1)))))
(newline)
