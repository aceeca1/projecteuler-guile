(display (let loop ((i 0) (answer 0))
    (if (= i 1000) answer
        (loop (+ i 1) (if
            (or (zero? (modulo i 3)) (zero? (modulo i 5)))
            (+ answer i) answer)))))
(newline)
