(display (let loop ((i 2) (answer 1))
    (if (< 20 i) answer (loop (+ i 1) (lcm answer i)))))
(newline)
