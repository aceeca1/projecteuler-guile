(use-modules (rnrs io ports))

(define (string-split s)
    (define (find-first-space k) (cond
        ((<= (string-length s) k) k)
        ((char=? (string-ref s k) #\space) k)
        (else (find-first-space (+ k 1)))))
    (define answer (list 'head))
    (let loop ((i 0) (tail answer))
        (if (<= (string-length s) i) (cdr answer)
            (let ((i2 (find-first-space i)))
                (set-cdr! tail (list (substring s i i2)))
                (loop (+ i2 1) (cdr tail))))))

(define (string->ints s)
    (list->vector (map string->number (string-split s))))

(define (main port)
    (define n 20)
    (define a (make-vector n))
    (define answer 0)
    (define (product-along i1 i2 direction)
        (define d1 (car direction))
        (define d2 (cadr direction))
        (let loop ((answer 1) (i 4) (i1 i1) (i2 i2)) (cond
            ((zero? i) answer)
            ((not (<= 0 i1 (- n 1))) 0)
            ((not (<= 0 i2 (- n 1))) 0)
            (else (loop
                (* answer (vector-ref (vector-ref a i1) i2))
                (- i 1) (+ i1 d1) (+ i2 d2))))))
    (let loop ((i 0))
        (if (< i n) (begin
            (vector-set! a i (string->ints (get-line port)))
            (loop (+ i 1)))))
    (let loop ((i1 0)) (if (< i1 n) (begin
        (let loop ((i2 0)) (if (< i2 n) (begin
            (let loop ((j '((0 1) (1 0) (1 1) (1 -1))))
                (if (pair? j) (begin
                    (let ((answer1 (product-along i1 i2 (car j))))
                        (set! answer (max answer answer1)))
                    (loop (cdr j)))))
            (loop (+ i2 1)))))
        (loop (+ i1 1)))))
    (display answer)
    (newline))

(call-with-input-file "data/input11.txt" main)
