(display (let loop ((i1 1) (i2 2) (answer 0))
    (if (< 4000000 i1) answer
        (loop i2 (+ i1 i2) (if (even? i1) (+ answer i1) answer)))))
(newline)
