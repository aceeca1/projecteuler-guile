(use-modules (rnrs io ports))

(define (main port)
    (define n 100)
    (define answer 0)
    (let loop ((i 0))
        (if (< i n) (begin
            (set! answer (+ answer (string->number (get-line port))))
            (loop (+ i 1)))))
    (display (substring (number->string answer) 0 10))
    (newline))

(call-with-input-file "data/input13.txt" main)
