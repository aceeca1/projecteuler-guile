(use-modules (srfi srfi-19))

(define (sunday? year month)
    (zero? (date-week-day (make-date 0 0 0 0 1 month year 0))))

(display (let loop ((i 1901) (answer 0)) (if (< 2000 i) answer
    (loop (+ i 1) (+ answer
        (let loop ((j 1) (answer 0)) (if (< 12 j) answer
            (loop (+ j 1) (if (sunday? i j) (+ answer 1) answer)))))))))
(newline)
