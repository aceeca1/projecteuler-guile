(define (pronounce n)
    (define x '#(
        "zero" "one" "two" "three" "four" "five" "six" "seven" "eight" "nine"
        "ten" "eleven" "twelve" "thirteen" "fourteen" "fifteen" "sixteen"
        "seventeen" "eighteen" "nineteen" "twenty"))
    (define x0 '#(
        "zero" "ten" "twenty" "thirty" "forty" "fifty" "sixty" "seventy"
        "eighty" "ninety"))
    (define (len x n) (string-length (vector-ref x n)))
    (define (pronounce2 n)
        (if (<= n 20) (len x n)
            (let ((n0 (modulo n 10)) (n1 (quotient n 10)))
                (+ (if (zero? n0) 0 (len x n0)) (len x0 n1)))))
    (cond
        ((< n 100) (pronounce2 n))
        ((= n 1000) (string-length "oneThousand"))
        (else (let ((n0 (modulo n 100)) (n2 (quotient n 100)))
            (+ (len x n2) (string-length "hundred")
                (if (zero? n0) 0 (+
                    (string-length "and")
                    (pronounce2 n0))))))))

(display (let loop ((i 1) (answer 0))
    (if (< 1000 i) answer (loop (+ i 1) (+ answer (pronounce i))))))
(newline)
